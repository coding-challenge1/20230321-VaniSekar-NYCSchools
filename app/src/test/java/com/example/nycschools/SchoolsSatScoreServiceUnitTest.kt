package com.example.nycschools

import com.example.nycschools.nycschools.api.SchoolsSatScoreApi
import com.example.nycschools.nycschools.model.SchoolSatScores
import com.example.nycschools.nycschools.service.SchoolsSatScoreService
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SchoolsSatScoreServiceUnitTest {

    @Mock
    private lateinit var schoolsSatScoreApi: SchoolsSatScoreApi

    private lateinit var schoolsSatScoreService: SchoolsSatScoreService
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    @Before
    fun setup() {
        schoolsSatScoreService =
            SchoolsSatScoreService(testScope, testDispatcher, schoolsSatScoreApi)
    }

    @After
    fun cleanup() {
        testScope.cleanupTestCoroutines()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test getAllSchoolsSatScores success`() = runBlockingTest {
        // Mock the response from the API
        val schoolSatScores = SchoolSatScores("1", "School A", "10", "100", "100", "100")
        val schoolSatScoresList = listOf(schoolSatScores)
        val schoolsSatScoresMap = mapOf(Pair("1", schoolSatScores))
        Mockito.`when`(schoolsSatScoreApi.getNycSchoolSatScores())
            .thenReturn(Single.just(schoolSatScoresList))

        // Call the function to be tested
        val result = runBlocking { schoolsSatScoreService.getSchoolSatScoresMap() }

        // Verify that the API was called
        Mockito.verify(schoolsSatScoreApi).getNycSchoolSatScores()

        // Verify that the returned result is correct
        assertEquals(schoolsSatScoresMap, result)
    }

    @Test
    fun `test getAllSchoolsSatScores error`() = runBlockingTest {
        // Mock the error response from the API
        val error = Throwable("Error")
        Mockito.`when`(schoolsSatScoreApi.getNycSchoolSatScores()).thenReturn(Single.error(error))

        // Call the function to be tested
        val result = runBlocking { schoolsSatScoreService.getSchoolSatScoresMap() }

        // Verify that the API was called
        Mockito.verify(schoolsSatScoreApi).getNycSchoolSatScores()

        // Verify that the returned result is empty
        assertTrue(result.isEmpty())
    }
}
