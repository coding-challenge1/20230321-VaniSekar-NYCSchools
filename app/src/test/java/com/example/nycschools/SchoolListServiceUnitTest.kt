package com.example.nycschools

import com.example.nycschools.nycschools.api.SchoolsListApi
import com.example.nycschools.nycschools.model.School
import com.example.nycschools.nycschools.service.SchoolsListService
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SchoolsListServiceTest {

    @Mock
    private lateinit var schoolsListApi: SchoolsListApi

    private lateinit var schoolsListService: SchoolsListService
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    @Before
    fun setup() {
        schoolsListService = SchoolsListService(testScope, testDispatcher, schoolsListApi)
    }

    @After
    fun cleanup() {
        testScope.cleanupTestCoroutines()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test getAllNycSchoolsList success`() = runBlockingTest {
        // Mock the response from the API
        val schools = listOf(School("1", "School A"), School("2", "School B"))
        Mockito.`when`(schoolsListApi.getNYCSchoolsList()).thenReturn(Single.just(schools))

        // Call the function to be tested
        val result = runBlocking { schoolsListService.getAllNycSchoolsList() }

        // Verify that the API was called
        Mockito.verify(schoolsListApi).getNYCSchoolsList()

        // Verify that the returned result is correct
        assertEquals(schools, result)
    }

    @Test
    fun `test getAllNycSchoolsList error`() = runBlockingTest {
        // Mock the error response from the API
        val error = Throwable("Error")
        Mockito.`when`(schoolsListApi.getNYCSchoolsList()).thenReturn(Single.error(error))

        // Call the function to be tested
        val result = runBlocking { schoolsListService.getAllNycSchoolsList() }

        // Verify that the API was called
        Mockito.verify(schoolsListApi).getNYCSchoolsList()

        // Verify that the returned result is empty
        assertTrue(result.isEmpty())
    }

}
