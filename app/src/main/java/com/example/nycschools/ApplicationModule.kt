package com.example.nycschools

import android.content.Context
import com.example.nycschools.nycschools.api.SchoolsListApi
import com.example.nycschools.nycschools.api.SchoolsSatScoreApi
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Singleton

// This ApplicationModule is added as modules parameter in the @Component annotation of the Application Component
@Module
class ApplicationModule() {

    // adding all dependencies that need to be provided with the @Provides annotation
    @Provides
    fun provideCoroutineScope(): CoroutineScope {
        return CoroutineScope(Dispatchers.Main)
    }

    @Provides
    fun provideCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.Default
    }

    // Using retrofit library RxJava adapterFactory and JacksonConverter for the api calls
    @Provides
    @Singleton
    fun provideSchoolsListApi(): SchoolsListApi = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us/")
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .addConverterFactory(JacksonConverterFactory.create())
        .build()
        .create(SchoolsListApi::class.java)


    @Provides
    @Singleton
    fun provideSchoolsSatScoreApi(): SchoolsSatScoreApi = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us/")
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .addConverterFactory(JacksonConverterFactory.create())
        .build()
        .create(SchoolsSatScoreApi::class.java)

}
