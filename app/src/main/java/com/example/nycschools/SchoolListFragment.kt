package com.example.nycschools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.databinding.FragmentSchoolListBinding
import com.example.nycschools.nycschools.model.OverallScores
import com.example.nycschools.nycschools.ui.SchoolViewAdapter
import com.example.nycschools.nycschools.ui.SchoolsListFragmentViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the schools list destination in the navigation.
 */
class SchoolListFragment : Fragment() {

    @Inject
    lateinit var schoolsListFragmentViewModel: SchoolsListFragmentViewModel

    @Inject
    lateinit var coroutineScope: CoroutineScope

    @Inject
    lateinit var coroutineDispatcher: CoroutineDispatcher

    private lateinit var recyclerView: RecyclerView

    init {
        Application.dependencies?.inject(this)
    }

    private var _binding: FragmentSchoolListBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentSchoolListBinding.inflate(inflater, container, false)

        DaggerApplicationComponent.create().inject(this)

        // attaching the viewModel

        binding.viewModel = schoolsListFragmentViewModel

        binding.executePendingBindings()

        return getViewFromBinding(binding)
    }

    fun getViewFromBinding(binding: ViewDataBinding): View {

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // getting the RecyclerView and the SchoolViewAdapter
        recyclerView = view.findViewById(R.id.recycler_view)
        val adapter = SchoolViewAdapter(listOf(), emptyMap(), OverallScores("", "", ""))
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // Launching the coroutine to get the schoolsList and schoolSatScores and overallScores data and update the recyclerView
        lifecycleScope.launch {
            val schoolsList = schoolsListFragmentViewModel.getNycSchoolsList().sortedBy { it.schoolName }
            val schoolSatScores = schoolsListFragmentViewModel.getNycSatScoresMap()
            val overallScores =
                schoolsListFragmentViewModel.getOverallScores(schoolSatScores.values.toList())
            adapter.updateData(schoolsList, schoolSatScores, overallScores)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}