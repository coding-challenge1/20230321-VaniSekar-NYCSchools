package com.example.nycschools.nycschools.api

import com.example.nycschools.nycschools.model.School
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface SchoolsListApi {

    @GET("resource/s3k6-pzi2.json")
    fun getNYCSchoolsList(): Single<List<School>>
}