package com.example.nycschools.nycschools.ui

import androidx.lifecycle.ViewModel
import com.example.nycschools.nycschools.service.SchoolsListService
import com.example.nycschools.nycschools.service.SchoolsSatScoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolsSatScoresFragmentViewModel @Inject constructor(
    private val schoolsSatScoreService: SchoolsSatScoreService
) : ViewModel() {

    init {

    }

}