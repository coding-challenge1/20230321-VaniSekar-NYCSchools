package com.example.nycschools.nycschools.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class School(
    @JsonProperty("dbn") val dbn: String,
    @JsonProperty("school_name") val schoolName: String
)