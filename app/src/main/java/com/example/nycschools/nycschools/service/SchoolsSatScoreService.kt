package com.example.nycschools.nycschools.service

import com.example.nycschools.nycschools.api.SchoolsSatScoreApi
import com.example.nycschools.nycschools.model.OverallScores
import com.example.nycschools.nycschools.model.SchoolSatScores
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class SchoolsSatScoreService @Inject constructor(
    private val coroutineScope: CoroutineScope,
    private val coroutineDispatcher: CoroutineDispatcher,
    private val schoolsSatScoreApi: SchoolsSatScoreApi
) {

    // Fetching the list of SAT scores for all Schools from a suspend function
    open suspend fun getSchoolSatScores(): List<SchoolSatScores> {
        val schoolSatScores = coroutineScope.async(coroutineDispatcher) {
            val schoolSatScores = getSchoolSatScoresFromApi()
            println("The Schools sat scores size -->" + schoolSatScores.size)
            schoolSatScores
        }.await()

        return schoolSatScores
    }

    // Fetching a map of school dbn as key and SchoolSatScores as value
    open suspend fun getSchoolSatScoresMap(): Map<String, SchoolSatScores> {
        var schoolSatScores = mapOf<String, SchoolSatScores>()
        try {
            schoolSatScores = coroutineScope.async(coroutineDispatcher) {
                val schoolSatScores = getNycSchoolSatScoresMapFromApi()
                println("The Schools sat scores size -->" + schoolSatScores.size)
                schoolSatScores
            }.await()
        } catch (e: Exception) {
            println(e)
        }

        return schoolSatScores
    }

    // Transforming the Single list of SchoolSatScores fetched from the api call to a map of school dbn as key and SchoolSatScores as value
    private fun getNycSchoolSatScoresMapFromApi(): Map<String, SchoolSatScores> {
        return schoolsSatScoreApi.getNycSchoolSatScores()
            .map { list -> list.associateBy(SchoolSatScores::dbn) }.blockingGet()
    }

    // Getting a list of SchoolsSatScores from api call
    private fun getSchoolSatScoresFromApi() =
        schoolsSatScoreApi.getNycSchoolSatScores().blockingGet()

    open fun getOverallScores(schoolSatScores: List<SchoolSatScores>): OverallScores {
        // Filtering out any SchoolSatScores with invalid scores
        val validScores = schoolSatScores.filter { it.isValidScore() }

        val readingScores = validScores.map { it.satReadingAverageScore.toInt() }
        val mathScores = validScores.map { it.satMathAverageScore.toInt() }
        val writingScores = validScores.map { it.satWritingAverageScore.toInt() }

        val readingAverage = readingScores.average().toInt().toString()
        val mathAverage = mathScores.average().toInt().toString()
        val writingAverage = writingScores.average().toInt().toString()

        return OverallScores(readingAverage, mathAverage, writingAverage)
    }


}