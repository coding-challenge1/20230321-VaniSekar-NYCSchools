package com.example.nycschools.nycschools.ui

import androidx.lifecycle.ViewModel
import com.example.nycschools.nycschools.model.SchoolSatScores
import com.example.nycschools.nycschools.service.SchoolsListService
import com.example.nycschools.nycschools.service.SchoolsSatScoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolsListFragmentViewModel @Inject constructor(
    private val schoolsListService: SchoolsListService,
    private val schoolsSatScoreService: SchoolsSatScoreService
): ViewModel() {

    open  suspend fun getNycSchoolsList() = schoolsListService.getAllNycSchoolsList()

    open  suspend fun getNycSatScoresMap() = schoolsSatScoreService.getSchoolSatScoresMap()

    open fun  getOverallScores(schoolSatScores: List<SchoolSatScores>) = schoolsSatScoreService.getOverallScores(schoolSatScores)

}