package com.example.nycschools.nycschools.api

import com.example.nycschools.nycschools.model.School
import com.example.nycschools.nycschools.model.SchoolSatScores
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface SchoolsSatScoreApi {

    @GET("resource/f9bf-2cp4.json")
    fun getNycSchoolSatScores(): Single<List<SchoolSatScores>>
}