package com.example.nycschools.nycschools.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
data class SchoolSatScores(
    @JsonProperty("dbn") val dbn: String,
    @JsonProperty("school_name") val schoolName: String,
    @JsonProperty("num_of_sat_test_takers") val noOfTestTakers: String,
    @JsonProperty("sat_critical_reading_avg_score") val satReadingAverageScore: String,
    @JsonProperty("sat_math_avg_score") val satMathAverageScore: String,
    @JsonProperty("sat_writing_avg_score") val satWritingAverageScore: String
) : Serializable
{
    fun isValidScore(): Boolean {
        return try {
            this.satMathAverageScore.toInt()
            this.satReadingAverageScore.toInt()
            this.satWritingAverageScore.toInt()
            true
        } catch (e: NumberFormatException) {
            false
        }
    }
}

data class OverallScores(
    val nycReadingAverage: String,
    val nycMathAverage: String,
    val nycWritingAverage: String
): Serializable