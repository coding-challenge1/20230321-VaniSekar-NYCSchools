package com.example.nycschools.nycschools.service

import com.example.nycschools.nycschools.api.SchoolsListApi
import com.example.nycschools.nycschools.model.School
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class SchoolsListService @Inject constructor(
    private val coroutineScope: CoroutineScope,
    private val coroutineDispatcher: CoroutineDispatcher,
    private val schoolsListApi: SchoolsListApi
) {

    open suspend fun getAllNycSchoolsList(): List<School> {
        var schoolsList = emptyList<School>()
        try {
            schoolsList = coroutineScope.async(coroutineDispatcher) {
                val schoolsListFromApi = getAllNycSchoolsListFromApi()
                schoolsListFromApi
            }.await()

        } catch (e: Exception) {
            println(e)
        }
        return schoolsList
    }

    private fun getAllNycSchoolsListFromApi() = schoolsListApi.getNYCSchoolsList().blockingGet()

}