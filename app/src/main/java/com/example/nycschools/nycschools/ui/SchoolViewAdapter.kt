package com.example.nycschools.nycschools.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.nycschools.model.OverallScores
import com.example.nycschools.nycschools.model.School
import com.example.nycschools.nycschools.model.SchoolSatScores

class SchoolViewAdapter(
    private var schools: List<School>,
    private var schoolSatScores: Map<String, SchoolSatScores>,
    private var overallScores: OverallScores
) : RecyclerView.Adapter<SchoolViewAdapter.SchoolViewHolder>() {


    inner class SchoolViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val schoolNameTextView: TextView = itemView.findViewById(R.id.text_view)

        init {
            itemView.setOnClickListener {
                // handle school click event
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    // getting the selected school
                    val school = schools[position]

                    // getting the selected schoolSatScores from the map using the school dbn which is stored as key in the map
                    // Sometimes the required dbn is not found in the list of schoolsSatScores in which case I am returning a schoolSatScore with the selected school dbn and satScores as Not Available
                    var schoolSatScore = schoolSatScores[school.dbn] ?: SchoolSatScores(
                        school.dbn,
                        school.schoolName,
                        "NA",
                        "NA",
                        "NA",
                        "NA"
                    )

                    // Navigating to the SchoolSatScoresFragment along with the selected schoolSatScore and overallScores passed as a bundle
                    findNavController(itemView).navigate(
                        R.id.action_SchoolListFragment_to_SchoolSatScoresFragment,
                        bundleOf(
                            "schoolSatScore" to schoolSatScore,
                            "overallScores" to overallScores
                        )
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.school, parent, false)
        return SchoolViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        val item = schools[position]
        val schoolName = item.schoolName
        // Setting the textview data as the school name
        holder.schoolNameTextView.text = schoolName
    }

    override fun getItemCount(): Int {
        return schools.size
    }

    fun updateData(
        schoolsList: List<School>,
        schoolSatScores: Map<String, SchoolSatScores>,
        overallScores: OverallScores
    ) {
        // updating the data received from the api call lists and notifying as the data gets updated for the recyclerview to be redrawn with the newly updated data
        this.schools = schoolsList
        this.schoolSatScores = schoolSatScores
        this.overallScores = overallScores
        notifyDataSetChanged()
    }
}
