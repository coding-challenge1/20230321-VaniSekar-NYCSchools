package com.example.nycschools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.example.nycschools.databinding.FragmentSchoolSatScoresBinding
import com.example.nycschools.nycschools.model.OverallScores
import com.example.nycschools.nycschools.model.SchoolSatScores
import com.example.nycschools.nycschools.ui.SchoolsSatScoresFragmentViewModel
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the SchoolSatScores destination in the navigation.
 */
class SchoolSatScoresFragment : Fragment() {

    @Inject
    lateinit var schoolsSatScoresFragmentViewModel: SchoolsSatScoresFragmentViewModel

    init {
        Application.dependencies?.inject(this)
    }

    private var _binding: FragmentSchoolSatScoresBinding? = null
    private val binding get() = _binding!!

    lateinit var barChart: BarChart
    lateinit var barDataSetOverallScores: BarDataSet
    lateinit var barDataSetSchoolSatScores: BarDataSet
    lateinit var barEntriesList: ArrayList<BarEntry>
    var subjects = arrayOf("Reading", "Math", "Writing")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentSchoolSatScoresBinding.inflate(inflater, container, false)

        DaggerApplicationComponent.create().inject(this)

        // Attaching the view model here although currently am not using it

        binding.viewModel = schoolsSatScoresFragmentViewModel

        binding.executePendingBindings()
        return getViewFromBinding(binding)
    }

    fun getViewFromBinding(binding: ViewDataBinding): View {

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // getting the schoolSatScore and overallScores passed as a bundle during navigation action in the SchoolViewAdapter

        val schoolSatScore = arguments?.getSerializable("schoolSatScore") as SchoolSatScores
        val overallScores = arguments?.getSerializable("overallScores") as OverallScores


        // Update UI with schoolSatScore data
        binding.textviewSchoolName.text = schoolSatScore.schoolName
        binding.textviewSatReadingScore.text = schoolSatScore.satReadingAverageScore
        binding.textviewSatMathScore.text = schoolSatScore.satMathAverageScore
        binding.textviewSatWritingScore.text = schoolSatScore.satWritingAverageScore

        // adding a barchart to compare the school's average scores against the nyc average scores

        barChart = view.findViewById(R.id.idBarChart)
        barDataSetOverallScores = BarDataSet(getBarChartDataForSet1(overallScores), "NYC Average")
        barDataSetOverallScores.setColor(resources.getColor(R.color.purple_200))
        barDataSetSchoolSatScores =
            BarDataSet(getBarChartDataForSet2(schoolSatScore), "This school Average")
        barDataSetSchoolSatScores.setColor(resources.getColor(R.color.teal_200))

        // would consider extracting the bar chart set up to a separate function if I had more time
        // adding bar data set to bar data
        var data = BarData(barDataSetOverallScores, barDataSetSchoolSatScores)
        barChart.data = data
        barChart.description.isEnabled = false

        // setting x axis and y axis scales
        var xAxis = barChart.xAxis
        var yAxisLeft = barChart.axisLeft
        yAxisLeft.axisMinimum = 0f
        var yAxisRight = barChart.axisRight
        yAxisRight.axisMinimum = 0f

        //adding our subjects to our x axis and configuring bar chart parameters
        xAxis.valueFormatter = IndexAxisValueFormatter(subjects)
        xAxis.setCenterAxisLabels(true)
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setGranularity(1f)
        xAxis.setGranularityEnabled(true)
        barChart.setDragEnabled(true)
        barChart.setVisibleXRangeMaximum(3f)
        val barSpace = 0.1f
        val groupSpace = 0.5f
        data.barWidth = 0.15f
        barChart.xAxis.axisMinimum = 0f
        barChart.animate()
        barChart.groupBars(0f, groupSpace, barSpace)
        barChart.invalidate()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun getBarChartDataForSet1(overallScores: OverallScores): ArrayList<BarEntry> {
        barEntriesList = ArrayList()
        barEntriesList.add(BarEntry(1f, overallScores.nycReadingAverage.toFloat()))
        barEntriesList.add(BarEntry(2f, overallScores.nycMathAverage.toFloat()))
        barEntriesList.add(BarEntry(3f, overallScores.nycWritingAverage.toFloat()))
        return barEntriesList
    }

    private fun getBarChartDataForSet2(schoolSatScore: SchoolSatScores): ArrayList<BarEntry> {

        barEntriesList = ArrayList()
        // Here I am assuming when one score is NA or s all scores are invalid and not available and hence not showing the barchart
        if (schoolSatScore.isValidScore()) {
            barEntriesList.add(BarEntry(1f, schoolSatScore.satReadingAverageScore.toFloat()))
            barEntriesList.add(BarEntry(2f, schoolSatScore.satMathAverageScore.toFloat()))
            barEntriesList.add(BarEntry(3f, schoolSatScore.satWritingAverageScore.toFloat()))
        }
        return barEntriesList
    }
}