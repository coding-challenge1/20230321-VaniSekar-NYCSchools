package com.example.nycschools

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(application: Application)

    fun inject(application: SchoolListFragment)

    fun inject(application: SchoolSatScoresFragment)

}