package com.example.nycschools

import androidx.multidex.MultiDexApplication

open class Application: MultiDexApplication() {

    companion object {
        var dependencies: ApplicationComponent? = null
    }

    override fun onCreate() {
        super.onCreate()
        dependencies?.inject(this)
    }
}