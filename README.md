# NYC School App

This is an Android application that accesses the NYC open data via API calls and lists the school names using a recycler view in a view holder pattern.The app uses MVVM architecture. When the user clicks on a school, the app displays the school's average SAT scores along with visualization using MPAndroidChart.

## Technologies Used

- Kotlin programming language
- Retrofit HTTP client for API calls
- Kotlin coroutines for asynchronous programming
- Dagger for dependency injection
- Gradle for managing dependencies
- JUnit and Mockito for unit testing


## Getting Started

To get started with the app, you can follow these steps:

1. Clone the repository: `git clone https://gitlab.com/coding-challenge1/20230321-VaniSekar-NYCSchools.git`
2. Open the project in Android Studio
3. Build and run the app on an emulator or a physical device

## Usage

When the app is launched, it displays a welcome screen with a button to get the list of NYC schools. On button click it displays a list of NYC schools. The user can scroll through the list to find a particular school. When the user clicks on a school, the app displays the school's average SAT scores along with a bar chart visualization comparing the school's scores with overall NYC schools average scores.

## Testing

The app comes with sample unit tests to ensure that the code works as expected. To run the tests, you can follow these steps:

1. Open the project in Android Studio
2. Navigate to the `app/src/test` directory
3. Right-click on a test file and select "Run"

## Demo

Below is a gif that demonstrates the functionality of the NYC School Android app:

![NYC School app demo](https://user-images.githubusercontent.com/48219167/226737576-6f15edde-ae5e-4597-9f61-a3cb74abe1a5.gif)
